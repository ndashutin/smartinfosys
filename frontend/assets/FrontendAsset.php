<?php
/**
 * @date 2017-04-29
 * @time 14:25
 * @author n.dashutin <mikola.dashutin@gmail.com>
 */

namespace frontend\assets;

use yii\bootstrap\BootstrapPluginAsset;
use yii\web\AssetBundle;
use yii\web\YiiAsset;

/**
 * Class FrontendAsset
 * @author n.dashutin <mikola.dashutin@gmail.com>
 * @package assets
 */
class FrontendAsset extends AssetBundle
{
    /**
     * @inheritdoc
     */
    public $basePath = '@webroot';
    /**
     * @inheritdoc
     */
    public $baseUrl = '@web';
    /**
     * @inheritdoc
     */
    public $css = [
        'css/main.css'
    ];
    /**
     * @inheritdoc
     */
    public $js = [

    ];
    /**
     * @inheritdoc
     */
    public $depends = [
        YiiAsset::class,
        BootstrapPluginAsset::class,
        \rmrevin\yii\fontawesome\AssetBundle::class
    ];
}