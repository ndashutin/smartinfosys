<?php

use yii\db\Migration;

class m170503_151859_user_auth_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('userAuthenticationKey', [
            'id' => $this->primaryKey(),
            'userId' => $this->integer()->notNull(),
            //'authProviderId' => $this->string()->notNull(),
            'providerUserId' => $this->string()->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('userAuthenticationKey');
    }
}
