<?php

use yii\db\Migration;

class m170429_132509_user_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('user',
            [
                'id' => $this->primaryKey(),
                'email' => $this->text()->notNull(),
                'password' => $this->text()->notNull(),
                'statusId' => $this->integer()->defaultValue(1)
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('user');
    }
}
