<?php
/**
 * @date 2017-04-29
 * @time 13:59
 * @author n.dashutin <mikola.dashutin@gmail.com>
 */

namespace frontend\controller;


use frontend\application\common\Factory\Entity\EntityFactoryInterface;
use frontend\application\domain\Aggregator\UserDataAggregatorInterface;
use frontend\application\domain\DTO\Authentication\AuthenticationDTOInterface;
use frontend\application\domain\DTO\Registration\RegistrationDTOInterface;
use frontend\application\exception\UserAlreadyRegisteredException;
use frontend\application\services\AuthenticationService\AuthenticationServiceInterface;
use frontend\application\services\RegistrationService\RegistrationServiceInterface;
use frontend\models\User\UserForm;
use frontend\models\User\UserIdentityObject;
use yii\base\Module;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\ErrorAction;
use yii\web\Response;

/**
 * Class FrontController
 * @author n.dashutin <mikola.dashutin@gmail.com>
 * @package frontend\controller
 */
class FrontController extends Controller
{
    /**
     * @var EntityFactoryInterface
     */
    private $authenticationDtoFactory;
    /**
     * @var EntityFactoryInterface
     */
    private $registrationDtoFactory;
    /**
     * @var AuthenticationServiceInterface
     */
    private $authenticationService;
    /**
     * @var RegistrationServiceInterface
     */
    private $registrationService;
    /**
     * @var UserDataAggregatorInterface
     */
    private $userDataAggregator;


    /**
     * FrontController constructor.
     * @param string $id
     * @param Module $module
     * @param EntityFactoryInterface $authenticationDtoFactory
     * @param EntityFactoryInterface $registrationDtoFactory
     * @param AuthenticationServiceInterface $authenticationService
     * @param RegistrationServiceInterface $registrationService
     * @param UserDataAggregatorInterface $userDataAggregator
     * @param array $config
     */
    public function __construct(
        $id,
        Module $module,
        EntityFactoryInterface $authenticationDtoFactory,
        EntityFactoryInterface $registrationDtoFactory,
        AuthenticationServiceInterface $authenticationService,
        RegistrationServiceInterface $registrationService,
        UserDataAggregatorInterface $userDataAggregator,
        array $config = []
    )
    {
        parent::__construct($id, $module, $config);
        $this->authenticationDtoFactory = $authenticationDtoFactory;
        $this->registrationDtoFactory = $registrationDtoFactory;
        $this->authenticationService = $authenticationService;
        $this->registrationService = $registrationService;
        $this->userDataAggregator = $userDataAggregator;
    }


    /**
     * @inheritdoc
     */
    public function behaviors(): array
    {
        $parentBehaviors = parent::behaviors();
        $currentBehaviors = [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'sign-up' => ['post'],
                    'sign-in' => ['post'],
                ],
            ],
        ];
        return array_merge($parentBehaviors, $currentBehaviors);
    }

    /**
     * @inheritdoc
     */
    public function actions(): array
    {
        return [
            'error' => [
                'class' => ErrorAction::class,
            ]
        ];
    }

    /**
     * Метод actionSignIn
     * @author n.dashutin <mikola.dashutin@gmail.com>
     * @return string|Response
     * @throws \yii\base\InvalidParamException
     */
    public function actionSignIn()
    {
        $userForm = $this->getUserForm();
        if ($userForm->load(\Yii::$app->getRequest()->post()) && $userForm->validate()) {
            /** @var AuthenticationDTOInterface $authenticationDtoEntity */
            $authenticationDtoEntity = $this->authenticationDtoFactory->makeEntity([
                'authenticationId' => $userForm->getAttribute('email'),
                'password' => $userForm->getAttribute('password')
            ]);
            $user = $this->authenticationService->authenticate($authenticationDtoEntity);
            if (null !== $user) {
                \Yii::$app->user->login(new UserIdentityObject($user));
                return $this->redirect(Url::toRoute('/'));
            }
            $userForm->addError('email', 'Invalid email address!');
        }
        return $this->actionIndex($userForm);
    }

    /**
     * Метод getUserForm
     * @author n.dashutin <mikola.dashutin@gmail.com>
     * @return UserForm
     */
    private function getUserForm(): UserForm
    {
        return new UserForm();
    }

    /**
     * Метод actionIndex
     * @author n.dashutin <mikola.dashutin@gmail.com>
     * @param UserForm|null $userForm
     * @param string $action
     * @return string
     */
    public function actionIndex(UserForm $userForm = null, $action = 'sign-in'): string
    {
        if (null === $userForm) {
            $userForm = $this->getUserForm();
        }
        return $this->render('index', ['form' => $userForm, 'action' => $action]);
    }

    /**
     * Метод actionSignOut
     * @author n.dashutin <mikola.dashutin@gmail.com>
     * @return Response
     * @throws \yii\base\InvalidParamException
     */
    public function actionSignOut(): Response
    {
        if (!\Yii::$app->user->isGuest) {
            \Yii::$app->user->logout();
        }
        return $this->redirect(Url::toRoute('/'));
    }

    /**
     * Метод actionSignUp
     * @author n.dashutin <mikola.dashutin@gmail.com>
     * @return string|Response
     */
    public function actionSignUp()
    {
        $userForm = $this->getUserForm();
        if ($userForm->load(\Yii::$app->getRequest()->post()) && $userForm->validate()) {
            /** @var RegistrationDTOInterface $registrationDtoEntity */
            $registrationDtoEntity = $this->registrationDtoFactory->makeEntity([
                'authenticationId' => $userForm->getAttribute('email'),
                'password' => $userForm->getAttribute('password')
            ]);
            try {
                $user = $this->registrationService->register($registrationDtoEntity);
                if (null !== $user) {
                    \Yii::$app->user->login(new UserIdentityObject($user));
                    return $this->redirect(Url::toRoute('/'));
                }
            } catch (UserAlreadyRegisteredException $exception) {
                $userForm->addError('email', 'This email is already in use!');
            }
        }
        return $this->actionIndex($userForm, 'sign-up');
    }

    /**
     * Метод actionDelete
     * @author n.dashutin <mikola.dashutin@gmail.com>
     * @return string
     */
    public function actionDelete(): string
    {
        $user = \Yii::$app->user;
        if (!$user->isGuest) {
            /** @var UserIdentityObject $identity */
            $identity = $user->getIdentity();
            $userEntity = $this->userDataAggregator->getUserByAuthenticationId($identity->getEmail());
            $this->userDataAggregator->deleteUser($userEntity);
            $user->logout();
        }
        return $this->actionIndex();
    }
}