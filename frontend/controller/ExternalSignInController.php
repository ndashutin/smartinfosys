<?php
/**
 * @date 2017-05-03
 * @time 17:29
 * @author n.dashutin <mikola.dashutin@gmail.com>
 */

namespace frontend\controller;

use frontend\application\common\Factory\Entity\EntityFactoryInterface;
use frontend\application\domain\DTO\Authentication\AuthenticationDTOInterface;
use frontend\application\exception\ApplicationException;
use frontend\application\exception\AuthenticationException;
use frontend\application\services\AuthenticationService\AuthenticationServiceInterface;
use frontend\models\User\UserIdentityObject;
use yii\authclient\AuthAction;
use yii\authclient\OAuth2;
use yii\base\Module;
use yii\web\BadRequestHttpException;
use yii\web\Controller;


/**
 * Class ExternalSignInController
 * @author n.dashutin <mikola.dashutin@gmail.com>
 * @package frontend\controller
 */
class ExternalSignInController extends Controller
{
    /**
     * @var AuthenticationServiceInterface
     */
    private $authenticationService;
    /**
     * @var EntityFactoryInterface
     */
    private $authenticationDTOFactory;

    /**
     * @inheritDoc
     */
    public function __construct(
        $id,
        Module $module,
        AuthenticationServiceInterface $authenticationService,
        EntityFactoryInterface $authenticationDTOFactory,
        array $config = []
    )
    {
        parent::__construct($id, $module, $config);
        $this->authenticationService = $authenticationService;
        $this->authenticationDTOFactory = $authenticationDTOFactory;
    }


    /**
     * @inheritdoc
     */
    public function actions(): array
    {
        return [
            'sign-in' => [
                'class' => AuthAction::class,
                'successCallback' => [$this, 'success'],
            ]
        ];
    }

    /**
     * Метод success
     * @author n.dashutin <mikola.dashutin@gmail.com>
     * @param OAuth2 $client
     * @throws BadRequestHttpException
     * @throws \frontend\application\exception\AuthenticationException
     * @throws \frontend\application\exception\ApplicationException
     */
    public function success(OAuth2 $client)
    {
        $userAttributes = $client->getUserAttributes();
        $userEmail = $userAttributes['email'] ?? null;
        if (null === $userEmail) {
            throw new BadRequestHttpException(
                'One of the required query attribute is missing! Attribute "email" is required!'
            );
        }
        $providerUserId = $userAttributes['id'] ?? null;
        if (null === $providerUserId) {
            throw new BadRequestHttpException(
                'One of the required query attribute is missing! Attribute "id" is required!'
            );
        }
        /** @var AuthenticationDTOInterface $authenticationEntity */
        $authenticationEntity = $this->authenticationDTOFactory->makeEntity(
            [
                'authenticationId' => $userEmail,
                'externalAuthenticationProviderToken' => $providerUserId
            ]
        );
        try {
            $user = $this->authenticationService->authenticateByExternalProvider($authenticationEntity);
        } catch (AuthenticationException $exception) {
            throw new ApplicationException('An error occurred during authentication!', null, $exception);
        }
        if (null !== $user) {
            \Yii::$app->user->login(new UserIdentityObject($user));
        } else {
            \Yii::$app->session->setFlash('error', 'Invalid email address!');
        }
    }


}