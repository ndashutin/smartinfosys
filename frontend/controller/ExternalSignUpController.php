<?php
/**
 * @date 2017-05-04
 * @time 01:05
 * @author n.dashutin <mikola.dashutin@gmail.com>
 */

namespace frontend\controller;

use frontend\application\common\Factory\Entity\EntityFactoryInterface;
use frontend\application\domain\DTO\Registration\RegistrationDTOInterface;
use frontend\application\exception\ApplicationException;
use frontend\application\exception\RegistrationException;
use frontend\application\services\RegistrationService\RegistrationServiceInterface;
use frontend\models\User\UserIdentityObject;
use yii\authclient\AuthAction;
use yii\authclient\OAuth2;
use yii\base\Module;
use yii\web\BadRequestHttpException;
use yii\web\Controller;

/**
 * Class ExternalSignUpController
 * @author n.dashutin <mikola.dashutin@gmail.com>
 * @package frontend\controller
 */
class ExternalSignUpController extends Controller
{
    /**
     * @var RegistrationServiceInterface
     */
    private $registrationService;
    /**
     * @var EntityFactoryInterface
     */
    private $registrationDTOFactory;

    /**
     * ExternalSignUpController constructor.
     * @param string $id
     * @param Module $module
     * @param RegistrationServiceInterface $registrationService
     * @param EntityFactoryInterface $authenticationDTOFactory
     * @param array $config
     */
    public function __construct(
        $id,
        Module $module,
        RegistrationServiceInterface $registrationService,
        EntityFactoryInterface $authenticationDTOFactory,
        array $config = []
    )
    {
        parent::__construct($id, $module, $config);
        $this->registrationService = $registrationService;
        $this->registrationDTOFactory = $authenticationDTOFactory;
    }


    /**
     * @inheritdoc
     */
    public function actions(): array
    {
        return [
            'sign-up' => [
                'class' => AuthAction::class,
                'successCallback' => [$this, 'success'],
            ]
        ];
    }

    /**
     * Метод success
     * @author n.dashutin <mikola.dashutin@gmail.com>
     * @param OAuth2 $client
     * @throws BadRequestHttpException
     * @throws \frontend\application\exception\AuthenticationException
     * @throws \frontend\application\exception\ApplicationException
     */
    public function success(OAuth2 $client)
    {
        $userAttributes = $client->getUserAttributes();
        $userEmail = $userAttributes['email'] ?? null;
        if (null === $userEmail) {
            throw new BadRequestHttpException(
                'One of the required query attribute is missing! Attribute "email" is required!'
            );
        }
        $providerUserId = $userAttributes['id'] ?? null;
        if (null === $providerUserId) {
            throw new BadRequestHttpException(
                'One of the required query attribute is missing! Attribute "id" is required!'
            );
        }
        /** @var RegistrationDTOInterface $authenticationEntity */
        $registrationDtoEntity = $this->registrationDTOFactory->makeEntity(
            [
                'authenticationId' => $userEmail,
                'externalAuthenticationProviderToken' => $providerUserId
            ]
        );
        try {
            $user = $this->registrationService->registerByExternalProvider($registrationDtoEntity);
        } catch (RegistrationException $exception) {
            throw new ApplicationException('An error occurred during registration!', null, $exception);
        }
        if (null !== $user) {
            \Yii::$app->user->login(new UserIdentityObject($user));
        } else {
            \Yii::$app->session->setFlash('error', 'An error occurred during registration!');
        }
    }
}