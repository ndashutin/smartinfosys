<?php
/**
 * @date 2017-05-05
 * @time 07:12
 * @author n.dashutin <mikola.dashutin@gmail.com>
 */

namespace frontend\application\domain\Entity\NotifyReceiver;

/**
 * Interface NotifyReceiverInterface
 * @author n.dashutin <mikola.dashutin@gmail.com>
 * @package frontend\application\domain\Entity\NotifyReceiver
 */
interface NotifyReceiverInterface
{
    /**
     * Метод getEmail
     * @author n.dashutin <mikola.dashutin@gmail.com>
     * @return string
     */
    public function getEmail(): string;
}