<?php
/**
 * @date 2017-05-03
 * @time 18:23
 * @author n.dashutin <mikola.dashutin@gmail.com>
 */

namespace frontend\application\domain\Entity\UserAuthenticationKey;

use frontend\application\common\Entity\EntityInterface;

/**
 * Class UserAuthenticationKeyEntity
 * @author n.dashutin <mikola.dashutin@gmail.com>
 * @package frontend\application\domain\UserAuthenticationKey\UserAuthKey
 */
class UserAuthenticationKeyEntity implements UserAuthenticationKeyEntityInterface, EntityInterface
{
    /**
     * @var int
     */
    private $id;
    /**
     * @var int
     */
    private $userId;
    /**
     * @var string
     */
    private $providerUserId;

    /**
     * @return int
     */
    public function getId(): int
    {
        return (int)$this->id;
    }

    /**
     * @inheritdoc
     */
    public function getUserId(): int
    {
        return (int)$this->userId;
    }

    /**
     * @return string
     */
    public function getAuthProviderId(): string
    {
        return '';
    }

    /**
     * @return string
     */
    public function getProviderUserId(): string
    {
        return (string)$this->providerUserId;
    }
}