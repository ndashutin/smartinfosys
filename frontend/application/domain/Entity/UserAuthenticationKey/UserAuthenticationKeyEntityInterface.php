<?php
/**
 * @date 2017-05-03
 * @time 18:26
 * @author n.dashutin <mikola.dashutin@gmail.com>
 */

namespace frontend\application\domain\Entity\UserAuthenticationKey;


/**
 * Class UserAuthenticationKeyEntityInterface
 * @author n.dashutin <mikola.dashutin@gmail.com>
 * @package frontend\application\domain\UserAuthenticationKey
 */
interface UserAuthenticationKeyEntityInterface
{
    /**
     * @return int
     */
    public function getId(): int;

    /**
     * @return int
     */
    public function getUserId(): int;

    /**
     * @return string
     */
    public function getAuthProviderId(): string;

    /**
     * @return string
     */
    public function getProviderUserId(): string;
}