<?php
/**
 * @date 2017-04-29
 * @time 15:16
 * @author n.dashutin <mikola.dashutin@gmail.com>
 */

namespace frontend\application\domain\Entity\User;

use frontend\application\common\Entity\EntityInterface;
use frontend\application\domain\Entity\NotifyReceiver\NotifyReceiverInterface;

/**
 * Class UserEntity
 * @author n.dashutin <mikola.dashutin@gmail.com>
 * @package frontend\application\domain\Entity\User
 */
class UserEntity implements UserInterface, EntityInterface, NotifyReceiverInterface
{
    /**
     * @var int
     */
    private $id;
    /**
     * @var string
     */
    private $email;
    /**
     * @var string
     */
    private $password;
    /**
     * @var int
     */
    private $statusId;

    /**
     * @return int
     */
    public function getId(): int
    {
        return (int)$this->id;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return (string)$this->email;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return (string)$this->password;
    }

    /**
     * @return int
     */
    public function getStatusId(): int
    {
        return (int)$this->statusId;
    }
}