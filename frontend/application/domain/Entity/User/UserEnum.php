<?php
/**
 * @date 2017-04-29
 * @time 15:17
 * @author n.dashutin <mikola.dashutin@gmail.com>
 */

namespace frontend\application\domain\Entity\User;

/**
 * Class UserEnum
 * @author n.dashutin <mikola.dashutin@gmail.com>
 * @package frontend\application\domain\Entity\User
 */
class UserEnum
{
    /**
     * @const int User active statusId
     */
    const STATUS_ACTIVE = 1;
    /**
     * @const int User deleted statusId
     */
    const STATUS_DELETED = 2;
}