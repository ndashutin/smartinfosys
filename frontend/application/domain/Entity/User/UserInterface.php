<?php
/**
 * @date 2017-04-29
 * @time 16:50
 * @author n.dashutin <mikola.dashutin@gmail.com>
 */

namespace frontend\application\domain\Entity\User;


/**
 * Class UserEntity
 * @author n.dashutin <mikola.dashutin@gmail.com>
 * @package frontend\application\domain\Entity\User
 */
interface UserInterface
{
    /**
     * @return int
     */
    public function getId(): int;

    /**
     * @return string
     */
    public function getEmail(): string;

    /**
     * @return string
     */
    public function getPassword(): string;

    /**
     * @return int
     */
    public function getStatusId(): int;
}