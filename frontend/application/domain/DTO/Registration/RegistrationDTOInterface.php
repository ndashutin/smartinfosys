<?php
/**
 * @date 2017-05-04
 * @time 13:54
 * @author n.dashutin <mikola.dashutin@gmail.com>
 */

namespace frontend\application\domain\DTO\Registration;


/**
 * Class RegistrationDTO
 * @author n.dashutin <mikola.dashutin@gmail.com>
 * @package frontend\application\domain\DTO\Registration
 */
interface RegistrationDTOInterface
{
    /**
     * @return string
     */
    public function getAuthenticationId(): string;

    /**
     * @return string
     */
    public function getPassword(): string;

    /**
     * @return string
     */
    public function getExternalAuthenticationProviderToken(): string;
}