<?php
/**
 * @date 2017-05-04
 * @time 13:53
 * @author n.dashutin <mikola.dashutin@gmail.com>
 */

namespace frontend\application\domain\DTO\Registration;

/**
 * Class RegistrationDTO
 * @author n.dashutin <mikola.dashutin@gmail.com>
 * @package frontend\application\domain\DTO\Registration
 */
class RegistrationDTO implements RegistrationDTOInterface
{
    /**
     * @var string
     */
    private $authenticationId;
    /**
     * @var string
     */
    private $password;
    /**
     * @var string
     */
    private $externalAuthenticationProviderToken;

    /**
     * @return string
     */
    public function getAuthenticationId(): string
    {
        return (string)$this->authenticationId;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return (string)$this->password;
    }

    /**
     * @return string
     */
    public function getExternalAuthenticationProviderToken(): string
    {
        return (string)$this->externalAuthenticationProviderToken;
    }
}