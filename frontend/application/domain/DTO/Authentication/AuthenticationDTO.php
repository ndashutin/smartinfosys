<?php
/**
 * @date 2017-05-03
 * @time 22:59
 * @author n.dashutin <mikola.dashutin@gmail.com>
 */

namespace frontend\application\domain\DTO\Authentication;

/**
 * Class AuthenticationDTO
 * @author n.dashutin <mikola.dashutin@gmail.com>
 * @package frontend\application\domain\DTO\Authentication
 */
class AuthenticationDTO implements AuthenticationDTOInterface
{
    /**
     * @var string
     */
    private $authenticationId;
    /**
     * @var string
     */
    private $password;
    /**
     * @var string
     */
    private $externalAuthenticationProviderToken;

    /**
     * @return string
     */
    public function getAuthenticationId(): string
    {
        return (string)$this->authenticationId;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return (string)$this->password;
    }

    /**
     * @return string
     */
    public function getExternalAuthenticationProviderToken(): string
    {
        return (string)$this->externalAuthenticationProviderToken;
    }
}