<?php
/**
 * @date 2017-05-03
 * @time 23:02
 * @author n.dashutin <mikola.dashutin@gmail.com>
 */

namespace frontend\application\domain\DTO\Authentication;


/**
 * Class AuthenticationDTO
 * @author n.dashutin <mikola.dashutin@gmail.com>
 * @package frontend\application\domain\DTO\Authentication
 */
interface AuthenticationDTOInterface
{
    /**
     * @return string
     */
    public function getAuthenticationId(): string;

    /**
     * @return string
     */
    public function getPassword(): string;

    /**
     * @return string
     */
    public function getExternalAuthenticationProviderToken(): string;
}