<?php
/**
 * @date 2017-05-04
 * @time 20:36
 * @author n.dashutin <mikola.dashutin@gmail.com>
 */

namespace frontend\application\domain\MessageBroker;

use frontend\application\domain\Job\MessageBrokerJobInterface;

/**
 * Interface MessageBroker
 * @author n.dashutin <mikola.dashutin@gmail.com>
 * @package frontend\application\domain\MessageBroker
 */
interface MessageBrokerInterface
{
    /**
     * Метод sendMessage
     * @author n.dashutin <mikola.dashutin@gmail.com>
     * @param MessageBrokerJobInterface $job
     */
    public function sendMessage(MessageBrokerJobInterface $job);
}