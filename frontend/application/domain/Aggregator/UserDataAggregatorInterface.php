<?php
/**
 * @date 2017-05-03
 * @time 23:41
 * @author n.dashutin <mikola.dashutin@gmail.com>
 */

namespace frontend\application\domain\Aggregator;

use frontend\application\domain\Entity\User\UserInterface;
use frontend\application\domain\Entity\UserAuthenticationKey\UserAuthenticationKeyEntityInterface;

/**
 * Interface UserDataAggregatorInterface
 * @author n.dashutin <mikola.dashutin@gmail.com>
 * @package frontend\application\domain\Aggregator
 */
interface UserDataAggregatorInterface
{
    /**
     * Метод getUserByAuthenticationId
     * @author n.dashutin <mikola.dashutin@gmail.com>
     * @param $authenticationId
     * @return UserInterface|null
     */
    public function getUserByAuthenticationId($authenticationId);

    /**
     * Метод getUserAuthenticationKey
     * @author n.dashutin <mikola.dashutin@gmail.com>
     * @param UserInterface $user
     * @param $externalAuthenticationProviderUserId
     * @return UserAuthenticationKeyEntityInterface|null
     */
    public function getUserAuthenticationKey(UserInterface $user, $externalAuthenticationProviderUserId);

    /**
     * Метод getUserByAuthenticationIdAndOpenPassword
     * @author n.dashutin <mikola.dashutin@gmail.com>
     * @param string $authenticationId
     * @param string $openPassword
     * @return UserInterface|null
     */
    public function getUserByAuthenticationIdAndOpenPassword($authenticationId, $openPassword);

    /**
     * Метод setNewUserAuthenticationKey
     * @author n.dashutin <mikola.dashutin@gmail.com>
     * @param UserInterface $user
     * @param $externalAuthenticationProviderUserId
     * @return UserAuthenticationKeyEntityInterface
     */
    public function setNewUserAuthenticationKey(UserInterface $user, $externalAuthenticationProviderUserId): UserAuthenticationKeyEntityInterface;

    /**
     * Метод setNewUser
     * @author n.dashutin <mikola.dashutin@gmail.com>
     * @param string $authenticationId
     * @param string|null $openPassword
     * @return UserInterface
     */
    public function setNewUser($authenticationId, $openPassword = null): UserInterface;

    /**
     * Метод deleteUser
     * @author n.dashutin <mikola.dashutin@gmail.com>
     * @param UserInterface $user
     * @return mixed
     */
    public function deleteUser(UserInterface $user);

    /**
     * Метод generateNewPassword
     * @author n.dashutin <mikola.dashutin@gmail.com>
     * @return string
     */
    public function generateNewPassword(): string;
}