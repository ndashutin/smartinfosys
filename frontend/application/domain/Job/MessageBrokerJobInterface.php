<?php
/**
 * @date 2017-05-04
 * @time 20:35
 * @author n.dashutin <mikola.dashutin@gmail.com>
 */

namespace frontend\application\domain\Job;

/**
 * Interface MessageBrokerJobInterface
 * @author n.dashutin <mikola.dashutin@gmail.com>
 * @package frontend\application\domain\Job
 */
interface MessageBrokerJobInterface
{
    /**
     * Метод setContext
     * @author n.dashutin <mikola.dashutin@gmail.com>
     * @param array $context
     */
    public function setContext(array $context);
}