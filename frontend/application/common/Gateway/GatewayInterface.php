<?php
/**
 * @date 2017-05-03
 * @time 21:23
 * @author n.dashutin <mikola.dashutin@gmail.com>
 */

namespace frontend\application\common\Gateway;

/**
 * Interface GatewayInterface
 * @author n.dashutin <mikola.dashutin@gmail.com>
 * @package frontend\application\common\Gateway
 */
interface GatewayInterface
{
    /**
     * Метод getOneByCriteria
     * @author n.dashutin <mikola.dashutin@gmail.com>
     * @param array $criteria
     * @return array
     */
    public function getOneByCriteria(array $criteria): array;

    /**
     * Метод getByCriteria
     * @author n.dashutin <mikola.dashutin@gmail.com>
     * @param array $criteria
     * @return array
     */
    public function getByCriteria(array $criteria = []): array;

    /**
     * Метод update
     * @author n.dashutin <mikola.dashutin@gmail.com>
     * @param array $data
     * @return bool
     */
    public function updateOne(array $data): bool;

    /**
     * Метод store
     * @author n.dashutin <mikola.dashutin@gmail.com>
     * @param array $data
     * @return bool
     */
    public function create(array $data): bool;

    /**
     * Метод remove
     * @author n.dashutin <mikola.dashutin@gmail.com>
     * @param array $criteria
     * @return bool
     */
    public function remove(array $criteria): bool;

}