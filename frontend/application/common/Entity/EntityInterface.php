<?php
/**
 * @date 2017-05-03
 * @time 21:57
 * @author n.dashutin <mikola.dashutin@gmail.com>
 */

namespace frontend\application\common\Entity;

/**
 * Interface EntityInterface
 * @author n.dashutin <mikola.dashutin@gmail.com>
 * @package frontend\application\common\Entity
 */
interface EntityInterface
{
    /**
     * Метод getId
     * @author n.dashutin <mikola.dashutin@gmail.com>
     * @return mixed
     */
    public function getId();
}