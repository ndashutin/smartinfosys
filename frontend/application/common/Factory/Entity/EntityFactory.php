<?php
/**
 * @date 2017-05-03
 * @time 20:58
 * @author n.dashutin <mikola.dashutin@gmail.com>
 */

namespace frontend\application\common\Factory\Entity;

use frontend\application\dao\EntityMapperInterface;

/**
 * Class EntityFactory
 * @author n.dashutin <mikola.dashutin@gmail.com>
 * @package frontend\application\common\Factory\Entity
 */
class EntityFactory implements EntityFactoryInterface
{
    /**
     * @var object
     */
    private $prototype;
    /**
     * @var EntityMapperInterface
     */
    private $entityMapper;

    /**
     * EntityFactory constructor.
     * @param $prototype
     * @param EntityMapperInterface $entityMapper
     */
    public function __construct($prototype, EntityMapperInterface $entityMapper)
    {
        $this->prototype = $prototype;
        $this->entityMapper = $entityMapper;
    }

    /**
     * Метод makeEntity
     * @author n.dashutin <mikola.dashutin@gmail.com>
     * @param array $entityData
     * @return mixed
     * @throws \InvalidArgumentException
     */
    public function makeEntity(array $entityData = [])
    {
        $entity = clone $this->prototype;
        return $this->entityMapper->map($entity, $entityData);
    }
}