<?php
/**
 * @date 2017-05-03
 * @time 21:02
 * @author n.dashutin <mikola.dashutin@gmail.com>
 */

namespace frontend\application\common\Factory\Entity;


/**
 * Class EntityFactory
 * @author n.dashutin <mikola.dashutin@gmail.com>
 * @package frontend\application\common\Factory\Entity
 */
interface EntityFactoryInterface
{
    /**
     * Метод makeEntity
     * @author n.dashutin <mikola.dashutin@gmail.com>
     * @param array $entityData
     * @return mixed
     */
    public function makeEntity(array $entityData = []);
}