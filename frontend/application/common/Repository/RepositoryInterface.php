<?php
/**
 * @date 2017-05-03
 * @time 20:53
 * @author n.dashutin <mikola.dashutin@gmail.com>
 */

namespace frontend\application\common\Repository;

use frontend\application\common\Entity\EntityInterface;

/**
 * Class RepositoryInterface
 * @author n.dashutin <mikola.dashutin@gmail.com>
 * @package frontend\application\common\Repository
 */
interface RepositoryInterface
{
    /**
     * Метод create
     * @author n.dashutin <mikola.dashutin@gmail.com>
     * @param $repositoryItem
     * @return mixed
     */
    public function create(EntityInterface $repositoryItem);

    /**
     * Метод update
     * @author n.dashutin <mikola.dashutin@gmail.com>
     * @param $repositoryItem
     * @return mixed
     */
    public function update(EntityInterface $repositoryItem);

    /**
     * Метод delete
     * @author n.dashutin <mikola.dashutin@gmail.com>
     * @param $repositoryItem
     * @return mixed
     */
    public function delete(EntityInterface $repositoryItem);

    /**
     * Метод makeEntityFromArray
     * @author n.dashutin <mikola.dashutin@gmail.com>
     * @param array $entityData
     * @return EntityInterface
     */
    public function makeEntityFromArray(array $entityData = []): EntityInterface;

    /**
     * Метод getOne
     * @author n.dashutin <mikola.dashutin@gmail.com>
     * @param array $criteria
     * @return mixed|null
     */
    public function getOne(array $criteria = []);

    /**
     * Метод getAll
     * @author n.dashutin <mikola.dashutin@gmail.com>
     * @param array $criteria
     * @return array
     */
    public function getAll(array $criteria = []): array;
}