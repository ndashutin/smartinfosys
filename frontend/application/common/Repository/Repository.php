<?php
/**
 * @date 2017-05-03
 * @time 20:54
 * @author n.dashutin <mikola.dashutin@gmail.com>
 */

namespace frontend\application\common\Repository;

use frontend\application\common\Entity\EntityInterface;
use frontend\application\common\Factory\Entity\EntityFactoryInterface;
use frontend\application\common\Gateway\GatewayInterface;
use frontend\application\dao\EntityMapperInterface;

/**
 * Class Repository
 * @author n.dashutin <mikola.dashutin@gmail.com>
 * @package frontend\application\common\Repository
 */
class Repository implements RepositoryInterface
{
    /**
     * @var GatewayInterface
     */
    private $entityGateway;
    /**
     * @var EntityMapperInterface
     */
    private $entityMapper;
    /**
     * @var EntityFactoryInterface
     */
    private $entityFactory;

    /**
     * Repository constructor.
     * @param GatewayInterface $entityGateway
     * @param EntityMapperInterface $entityMapper
     * @param EntityFactoryInterface $entityFactory
     */
    public function __construct(
        GatewayInterface $entityGateway,
        EntityMapperInterface $entityMapper,
        EntityFactoryInterface $entityFactory
    )
    {
        $this->entityGateway = $entityGateway;
        $this->entityMapper = $entityMapper;
        $this->entityFactory = $entityFactory;
    }


    /**
     * Метод create
     * @author n.dashutin <mikola.dashutin@gmail.com>
     * @param $repositoryItem
     * @return mixed
     * @throws \InvalidArgumentException
     */
    public function create(EntityInterface $repositoryItem)
    {
        return $this->entityGateway->create($this->entityMapper->retrieveData($repositoryItem));
    }

    /**
     * Метод update
     * @author n.dashutin <mikola.dashutin@gmail.com>
     * @param $repositoryItem
     * @return mixed
     * @throws \InvalidArgumentException
     */
    public function update(EntityInterface $repositoryItem)
    {
        return $this->entityGateway->updateOne($this->entityMapper->retrieveData($repositoryItem));
    }

    /**
     * Метод delete
     * @author n.dashutin <mikola.dashutin@gmail.com>
     * @param $repositoryItem
     * @return bool
     */
    public function delete(EntityInterface $repositoryItem): bool
    {
        return $this->entityGateway->remove(['id' => $repositoryItem]);
    }

    /**
     * Метод getOne
     * @author n.dashutin <mikola.dashutin@gmail.com>
     * @param array $criteria
     * @return mixed|null
     */
    public function getOne(array $criteria = [])
    {
        $entityDataArray = $this->entityGateway->getOneByCriteria($criteria);
        if (is_array($entityDataArray) && count($entityDataArray)) {
            return $this->makeEntityFromArray($entityDataArray);
        }
        return null;
    }

    /**
     * Метод makeEntityFromArray
     * @author n.dashutin <mikola.dashutin@gmail.com>
     * @param array $entityData
     * @return mixed
     */
    public function makeEntityFromArray(array $entityData = []): EntityInterface
    {
        return $this->entityFactory->makeEntity($entityData);
    }

    /**
     * Метод getAll
     * @author n.dashutin <mikola.dashutin@gmail.com>
     * @param array $criteria
     * @return array
     */
    public function getAll(array $criteria = []): array
    {
        $result = [];
        $entitiesDataArray = $this->entityGateway->getOneByCriteria($criteria);
        if (is_array($entitiesDataArray) && count($entitiesDataArray)) {
            foreach ($entitiesDataArray as $entityDataArray) {
                $result[] = $this->makeEntityFromArray($entityDataArray);
            }
        }
        return $result;
    }
}