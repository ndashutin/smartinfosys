<?php
/**
 * @date 2017-05-04
 * @time 00:05
 * @author n.dashutin <mikola.dashutin@gmail.com>
 */

namespace frontend\application\services\PasswordService;

/**
 * interface PasswordServiceInterface
 * @author n.dashutin <mikola.dashutin@gmail.com>
 * @package frontend\application\services\PasswordService
 */
interface PasswordServiceInterface
{
    /**
     * Метод generateRandom
     * @author n.dashutin <mikola.dashutin@gmail.com>
     * @param int $length
     * @param bool $letters
     * @param bool $numbers
     * @param bool $uppercase
     * @return string
     */
    public function generateRandom($length = 10, $letters = true, $numbers = true, $uppercase = true): string;

    /**
     * Метод makePasswordHash
     * @author n.dashutin <mikola.dashutin@gmail.com>
     * @param string $passwordString
     * @return string
     */
    public function makePasswordHash($passwordString): string;

    /**
     * Метод isEqual
     * @author n.dashutin <mikola.dashutin@gmail.com>
     * @param string $firstPasswordHash
     * @param string $secondPasswordHash
     * @return bool
     */
    public function isEqual($firstPasswordHash, $secondPasswordHash): bool;
}