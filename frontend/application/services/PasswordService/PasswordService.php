<?php
/**
 * @date 2017-05-04
 * @time 00:10
 * @author n.dashutin <mikola.dashutin@gmail.com>
 */

namespace frontend\application\services\PasswordService;

/**
 * Class PasswordService
 * @author n.dashutin <mikola.dashutin@gmail.com>
 * @package frontend\application\services\PasswordService
 */
class PasswordService implements PasswordServiceInterface
{
    /**
     * @inheritDoc
     */
    public function generateRandom($length = 12, $letters = true, $numbers = true, $uppercase = true): string
    {
        $source = '';
        $lowercaseLettersString = 'abcdefghijklmnopqrstuvwxyz';
        $uppercaseLettersString = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $numbersString = '0123456789';
        if ($letters) {
            $source .= $lowercaseLettersString;
        }
        if ($numbers) {
            $source .= $numbersString;
        }
        if ($uppercase) {
            $source .= $uppercaseLettersString;
        }
        return substr(str_shuffle(str_repeat($source, ceil($length / strlen($source)))), 1, $length);

    }

    /**
     * @inheritDoc
     */
    public function makePasswordHash($passwordString): string
    {
        return md5(sha1(md5($passwordString)));
    }

    /**
     * @inheritDoc
     */
    public function isEqual($firstPasswordHash, $secondPasswordHash): bool
    {
        return $firstPasswordHash === $secondPasswordHash;
    }

}