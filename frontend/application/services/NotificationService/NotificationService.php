<?php
/**
 * @date 2017-05-04
 * @time 20:30
 * @author n.dashutin <mikola.dashutin@gmail.com>
 */

namespace frontend\application\services\NotificationService;

use frontend\application\domain\Entity\NotifyReceiver\NotifyReceiverInterface;
use frontend\application\domain\MessageBroker\MessageBrokerInterface;
use frontend\application\services\NotificationService\Email\EmailTemplateProviderInterface;
use frontend\models\Job\MessageBrokerJobFactoryInterface;

/**
 * Class NotificationService
 * @author n.dashutin <mikola.dashutin@gmail.com>
 * @package frontend\application\services\NotificationService
 */
class NotificationService implements NotificationServiceInterface
{
    /**
     * @var MessageBrokerInterface
     */
    private $messageBroker;
    /**
     * @var MessageBrokerJobFactoryInterface
     */
    private $jobFactory;
    /**
     * @var EmailTemplateProviderInterface
     */
    private $emailTemplateProvider;

    /**
     * NotificationService constructor.
     * @param MessageBrokerInterface $messageBroker
     * @param MessageBrokerJobFactoryInterface $jobFactory
     * @param EmailTemplateProviderInterface $emailTemplateProvider
     */
    public function __construct(
        MessageBrokerInterface $messageBroker,
        MessageBrokerJobFactoryInterface $jobFactory,
        EmailTemplateProviderInterface $emailTemplateProvider
    )
    {
        $this->messageBroker = $messageBroker;
        $this->jobFactory = $jobFactory;
        $this->emailTemplateProvider = $emailTemplateProvider;
    }


    /**
     * Метод emailNotify
     * @author n.dashutin <mikola.dashutin@gmail.com>
     * @param $type
     * @param NotifyReceiverInterface $receiver
     * @param array $data
     * @throws \frontend\application\exception\UndefinedMessageBrokerJobTypeException
     * @throws \InvalidArgumentException
     * @throws \frontend\application\exception\UndefinedEmailTemplateException
     */
    public function emailNotify($type, NotifyReceiverInterface $receiver, array $data = [])
    {
        $template = $this->emailTemplateProvider->provide($type);
        if (count($data)) {
            $template->build($data);
        }
        $job = $this->jobFactory->makeJob($type);
        $job->setContext(
            [
                'receiver' => $receiver->getEmail(),
                'body' => $template->getBody(),
                'title' => $template->getTitle()
            ]
        );
        $this->messageBroker->sendMessage($job);
    }

}