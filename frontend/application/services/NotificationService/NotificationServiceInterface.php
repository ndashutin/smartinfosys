<?php
/**
 * @date 2017-05-05
 * @time 07:26
 * @author n.dashutin <mikola.dashutin@gmail.com>
 */

namespace frontend\application\services\NotificationService;

use frontend\application\domain\Entity\NotifyReceiver\NotifyReceiverInterface;


/**
 * Class NotificationService
 * @author n.dashutin <mikola.dashutin@gmail.com>
 * @package frontend\application\services\NotificationService
 */
interface NotificationServiceInterface
{
    /**
     * Метод emailNotify
     * @author n.dashutin <mikola.dashutin@gmail.com>
     * @param $type
     * @param NotifyReceiverInterface $receiver
     * @param array $data
     * @throws \frontend\application\exception\UndefinedMessageBrokerJobTypeException
     * @throws \InvalidArgumentException
     * @throws \frontend\application\exception\UndefinedEmailTemplateException
     */
    public function emailNotify($type, NotifyReceiverInterface $receiver, array $data = []);
}