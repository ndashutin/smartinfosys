<?php
/**
 * @date 2017-05-05
 * @time 07:06
 * @author n.dashutin <mikola.dashutin@gmail.com>
 */

namespace frontend\application\services\NotificationService\Email\Template;

/**
 * Interface EmailTemplateInterface
 * @author n.dashutin <mikola.dashutin@gmail.com>
 * @package frontend\application\services\NotificationService\Email\Template
 */
interface EmailTemplateInterface
{
    /**
     * Метод build
     * @author n.dashutin <mikola.dashutin@gmail.com>
     * @param array $contextArray
     */
    public function build(array $contextArray = []);

    /**
     * @return string
     */
    public function getBody(): string;

    /**
     * @return string
     */
    public function getTitle(): string;
}