<?php
/**
 * @date 2017-05-04
 * @time 20:47
 * @author n.dashutin <mikola.dashutin@gmail.com>
 */

namespace frontend\application\services\NotificationService\Email;

use frontend\application\services\NotificationService\Email\Template\EmailTemplateInterface;

/**
 * Interface EmailTemplateProviderInterface
 * @author n.dashutin <mikola.dashutin@gmail.com>
 * @package frontend\application\services\NotificationService\Email
 */
interface EmailTemplateProviderInterface
{
    /**
     * Метод provide
     * @author n.dashutin <mikola.dashutin@gmail.com>
     * @param $emailTemplateTitle
     * @return EmailTemplateInterface
     * @throws \frontend\application\exception\UndefinedEmailTemplateException
     */
    public function provide($emailTemplateTitle): EmailTemplateInterface;
}