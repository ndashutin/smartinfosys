<?php
/**
 * @date 2017-05-03
 * @time 18:32
 * @author n.dashutin <mikola.dashutin@gmail.com>
 */

namespace frontend\application\services\AuthenticationService;

use frontend\application\domain\Aggregator\UserDataAggregatorInterface;
use frontend\application\domain\DTO\Authentication\AuthenticationDTOInterface;

/**
 * Class AuthenticationService
 * @author n.dashutin <mikola.dashutin@gmail.com>
 * @package frontend\application\services\AuthenticationService
 */
class AuthenticationService implements AuthenticationServiceInterface
{
    /**
     * @var UserDataAggregatorInterface
     */
    private $userDataProvider;

    /**
     * AuthenticationService constructor.
     * @param UserDataAggregatorInterface $dataProvider
     */
    public function __construct(UserDataAggregatorInterface $dataProvider)
    {
        $this->userDataProvider = $dataProvider;
    }

    /**
     * @inheritdoc
     */
    public function authenticateByExternalProvider(AuthenticationDTOInterface $authenticationDTO)
    {
        $user = $this->userDataProvider->getUserByAuthenticationId($authenticationDTO->getAuthenticationId());
        if (null !== $user) {
            $authenticationKey = $this->userDataProvider->getUserAuthenticationKey(
                $user,
                $authenticationDTO->getExternalAuthenticationProviderToken()
            );
            if (null === $authenticationKey) {
                return null;
            }
        }
        return $user;
    }

    /**
     * @inheritdoc
     */
    public function authenticate(AuthenticationDTOInterface $authenticationDTO)
    {
        return $this->userDataProvider->getUserByAuthenticationIdAndOpenPassword(
            $authenticationDTO->getAuthenticationId(),
            $authenticationDTO->getPassword()
        );
    }
}