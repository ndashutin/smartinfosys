<?php
/**
 * @date 2017-05-03
 * @time 18:53
 * @author n.dashutin <mikola.dashutin@gmail.com>
 */

namespace frontend\application\services\AuthenticationService;

use frontend\application\domain\DTO\Authentication\AuthenticationDTOInterface;
use frontend\application\domain\Entity\User\UserInterface;


/**
 * Class AuthenticationService
 * @author n.dashutin <mikola.dashutin@gmail.com>
 * @package frontend\application\services\AuthenticationService
 */
interface AuthenticationServiceInterface
{
    /**
     * Метод authenticateByExternalUserIdProvider
     * @author n.dashutin <mikola.dashutin@gmail.com>
     * @param AuthenticationDTOInterface $authenticationDTO
     * @return UserInterface|null
     */
    public function authenticateByExternalProvider(AuthenticationDTOInterface $authenticationDTO);

    /**
     * Метод authenticate
     * @author n.dashutin <mikola.dashutin@gmail.com>
     * @param AuthenticationDTOInterface $authenticationDTO
     * @return UserInterface|null
     */
    public function authenticate(AuthenticationDTOInterface $authenticationDTO);
}