<?php
/**
 * @date 2017-05-04
 * @time 14:41
 * @author n.dashutin <mikola.dashutin@gmail.com>
 */

namespace frontend\application\services\RegistrationService;

use frontend\application\domain\DTO\Registration\RegistrationDTOInterface;
use frontend\application\domain\Entity\User\UserInterface;


/**
 * Class RegistrationService
 * @author n.dashutin <mikola.dashutin@gmail.com>
 * @package frontend\application\services\RegistrationService
 */
interface RegistrationServiceInterface
{
    /**
     * Метод registerByExternalProvider
     * @author n.dashutin <mikola.dashutin@gmail.com>
     * @param RegistrationDTOInterface $registrationDTO
     * @return UserInterface
     */
    public function registerByExternalProvider(RegistrationDTOInterface $registrationDTO): UserInterface;

    /**
     * Метод register
     * @author n.dashutin <mikola.dashutin@gmail.com>
     * @param RegistrationDTOInterface $registrationDTO
     * @return UserInterface
     */
    public function register(RegistrationDTOInterface $registrationDTO): UserInterface;
}