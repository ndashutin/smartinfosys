<?php
/**
 * @date 2017-05-04
 * @time 13:50
 * @author n.dashutin <mikola.dashutin@gmail.com>
 */

namespace frontend\application\services\RegistrationService;

use frontend\application\domain\Aggregator\UserDataAggregatorInterface;
use frontend\application\domain\DTO\Registration\RegistrationDTOInterface;
use frontend\application\domain\Entity\User\UserInterface;
use frontend\application\exception\UserAlreadyRegisteredException;
use frontend\application\services\NotificationService\NotificationServiceInterface;

/**
 * Class RegistrationService
 * @author n.dashutin <mikola.dashutin@gmail.com>
 * @package frontend\application\services\RegistrationService
 */
class RegistrationService implements RegistrationServiceInterface
{
    /**
     * @var UserDataAggregatorInterface
     */
    private $userDataAggregator;
    /**
     * @var NotificationServiceInterface
     */
    private $notificationService;

    /**
     * RegistrationService constructor.
     * @param UserDataAggregatorInterface $userDataAggregator
     * @param NotificationServiceInterface $notificationService
     */
    public function __construct(
        UserDataAggregatorInterface $userDataAggregator,
        NotificationServiceInterface $notificationService
    )
    {
        $this->userDataAggregator = $userDataAggregator;
        $this->notificationService = $notificationService;
    }

    /**
     * Метод registerByExternalProvider
     * @author n.dashutin <mikola.dashutin@gmail.com>
     * @param RegistrationDTOInterface $registrationDTO
     * @return UserInterface
     * @throws \frontend\application\exception\UserAlreadyRegisteredException
     */
    public function registerByExternalProvider(RegistrationDTOInterface $registrationDTO): UserInterface
    {
        $this->register($registrationDTO);
        $user = $this->processNewAuthenticationKey(
            $registrationDTO->getAuthenticationId(),
            $registrationDTO->getExternalAuthenticationProviderToken()
        );
        return $user;
    }

    /**
     * Метод register
     * @author n.dashutin <mikola.dashutin@gmail.com>
     * @param RegistrationDTOInterface $registrationDTO
     * @return UserInterface
     * @throws \frontend\application\exception\UserAlreadyRegisteredException
     */
    public function register(RegistrationDTOInterface $registrationDTO): UserInterface
    {
        $authenticationId = $registrationDTO->getAuthenticationId();
        $openPassword = $registrationDTO->getPassword();
        if (!$this->userAlreadyRegistered($authenticationId)) {
            $this->processNewUser($authenticationId, $openPassword);
        } else {
            if (empty($registrationDTO->getExternalAuthenticationProviderToken())) {
                throw new UserAlreadyRegisteredException(
                    'User with authentication id "' . $authenticationId . '" already registered!'
                );
            }
        }
        return $this->userDataAggregator->getUserByAuthenticationId($authenticationId);
    }

    /**
     * Метод userAlreadyRegistered
     * @author n.dashutin <mikola.dashutin@gmail.com>
     * @param string $authenticationId
     * @return bool
     */
    private function userAlreadyRegistered($authenticationId): bool
    {
        return $this->userDataAggregator->getUserByAuthenticationId($authenticationId) !== null;
    }

    /**
     * Метод processNewUser
     * @author n.dashutin <mikola.dashutin@gmail.com>
     * @param string $authenticationId
     * @param string $openPassword
     * @return UserInterface
     * @throws \frontend\application\exception\UndefinedMessageBrokerJobTypeException
     * @throws \frontend\application\exception\UndefinedEmailTemplateException
     * @throws \InvalidArgumentException
     */
    private function processNewUser($authenticationId, $openPassword): UserInterface
    {
        if (empty($openPassword)) {
            $openPassword = $this->userDataAggregator->generateNewPassword();
        }
        $newUser = $this->userDataAggregator->setNewUser($authenticationId, $openPassword);
        $this->notificationService->emailNotify(
            'afterUserRegistration',
            $newUser,
            [
                'authenticateId' => $authenticationId,
                'password' => $openPassword
            ]
        );
        return $newUser;
    }

    /**
     * Метод processNewAuthenticationKey
     * @author n.dashutin <mikola.dashutin@gmail.com>
     * @param string $authenticationId
     * @param string $externalAuthenticationProviderToken
     * @return UserInterface
     */
    private function processNewAuthenticationKey($authenticationId, $externalAuthenticationProviderToken): UserInterface
    {
        $user = $this->userDataAggregator->getUserByAuthenticationId($authenticationId);
        if (!$this->userAlreadyHasAuthenticationKey($user, $externalAuthenticationProviderToken)) {
            $this->userDataAggregator->setNewUserAuthenticationKey($user, $externalAuthenticationProviderToken);
        }
        return $user;
    }

    /**
     * Метод userAlreadyHasAuthenticationKey
     * @author n.dashutin <mikola.dashutin@gmail.com>
     * @param UserInterface $user
     * @param $externalAuthenticationProviderToken
     * @return bool
     */
    private function userAlreadyHasAuthenticationKey(UserInterface $user, $externalAuthenticationProviderToken): bool
    {
        return $this->userDataAggregator->getUserAuthenticationKey($user, $externalAuthenticationProviderToken) !== null;
    }
}