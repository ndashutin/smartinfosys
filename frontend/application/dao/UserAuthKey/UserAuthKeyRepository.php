<?php
/**
 * @date 2017-05-03
 * @time 18:14
 * @author n.dashutin <mikola.dashutin@gmail.com>
 */

namespace frontend\application\dao\UserAuthKey;

use frontend\application\common\Repository\Repository;

/**
 * Class UserAuthKeyMapper
 * @author n.dashutin <mikola.dashutin@gmail.com>
 * @package frontend\application\dao\UserAuthKey
 */
class UserAuthKeyRepository extends Repository
{

}