<?php
/**
 * @date 2017-04-29
 * @time 15:14
 * @author n.dashutin <mikola.dashutin@gmail.com>
 */

namespace frontend\application\dao\User;

use frontend\application\common\Repository\Repository;

/**
 * Class UserMapper
 * @author n.dashutin <mikola.dashutin@gmail.com>
 * @package frontend\dao\User
 */
class UserRepository extends Repository
{

}