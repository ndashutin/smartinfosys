<?php
/**
 * @date 2017-04-29
 * @time 15:47
 * @author n.dashutin <mikola.dashutin@gmail.com>
 */

namespace frontend\application\dao;

/**
 * Class EntityMapper
 * @author n.dashutin <mikola.dashutin@gmail.com>
 * @package frontend\application\dao
 */
class EntityMapper implements EntityMapperInterface
{
    /**
     * @inheritdoc
     */
    public function map($entity, array $dataForeMapping)
    {
        $this->checkEntity($entity);
        $mappingCallable = function (array $dataForeMapping) {
            foreach ($dataForeMapping as $propertyName => $propertyValue) {
                if (property_exists($this, $propertyName)) {
                    $this->$propertyName = $propertyValue;
                }
            }
            return $this;
        };
        $objectInit = $mappingCallable->bindTo($entity, $entity);
        return $objectInit($dataForeMapping);
    }

    /**
     * Метод checkEntity
     * @author n.dashutin <mikola.dashutin@gmail.com>
     * @param $entity
     * @throws \InvalidArgumentException
     */
    private function checkEntity($entity)
    {
        if (!is_object($entity)) {
            throw new \InvalidArgumentException('Parameter for mapping is not object!');
        }
    }

    /**
     * @inheritdoc
     */
    public function retrieveData($entity)
    {
        $this->checkEntity($entity);
        $mappingCallable = function () {
            $data = [];
            foreach (get_class_vars(get_class($this)) as $field => $value) {
                $data[$field] = $this->$field;
            }
            return $data;
        };
        $retrievedData = $mappingCallable->bindTo($entity, $entity);
        return $retrievedData();
    }
}