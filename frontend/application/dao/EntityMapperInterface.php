<?php
/**
 * @date 2017-04-29
 * @time 15:47
 * @author n.dashutin <mikola.dashutin@gmail.com>
 */

namespace frontend\application\dao;

/**
 * Class EntityMapper
 * @author n.dashutin <mikola.dashutin@gmail.com>
 * @package frontend\application\dao
 */
interface EntityMapperInterface
{
    /**
     * Метод mapEntity
     * @author n.dashutin <mikola.dashutin@gmail.com>
     * @param $entity
     * @param array $dataForeMapping
     * @return mixed
     * @throws \InvalidArgumentException
     */
    public function map($entity, array $dataForeMapping);

    /**
     * Метод retrieveDataFromEntity
     * @author n.dashutin <mikola.dashutin@gmail.com>
     * @param $entity
     * @return mixed
     * @throws \InvalidArgumentException
     */
    public function retrieveData($entity);
}