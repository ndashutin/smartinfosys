<?php
/**
 * @date 2017-05-03
 * @time 18:04
 * @author n.dashutin <mikola.dashutin@gmail.com>
 */

namespace frontend\application\exception;

/**
 * Class AuthenticationException
 * @author n.dashutin <mikola.dashutin@gmail.com>
 * @package frontend\application\exception
 */
class AuthenticationException extends ApplicationException
{

}