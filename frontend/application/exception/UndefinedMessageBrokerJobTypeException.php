<?php
/**
 * @date 2017-05-04
 * @time 20:44
 * @author n.dashutin <mikola.dashutin@gmail.com>
 */

namespace frontend\application\exception;

/**
 * Class UndefinedMessageBrokerJobTypeException
 * @author n.dashutin <mikola.dashutin@gmail.com>
 * @package frontend\application\exception
 */
class UndefinedMessageBrokerJobTypeException extends ApplicationException
{

}