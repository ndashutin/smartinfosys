<?php
/**
 * @date 2017-05-04
 * @time 13:56
 * @author n.dashutin <mikola.dashutin@gmail.com>
 */

namespace frontend\application\exception;

/**
 * Class RegistrationException
 * @author n.dashutin <mikola.dashutin@gmail.com>
 * @package frontend\application\exception
 */
class RegistrationException extends ApplicationException
{

}