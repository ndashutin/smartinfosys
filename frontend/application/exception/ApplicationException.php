<?php
/**
 * @date 2017-05-03
 * @time 21:41
 * @author n.dashutin <mikola.dashutin@gmail.com>
 */

namespace frontend\application\exception;

/**
 * Class ApplicationException
 * @author n.dashutin <mikola.dashutin@gmail.com>
 * @package frontend\application\exception
 */
class ApplicationException extends \LogicException
{

}