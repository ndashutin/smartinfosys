<?php
/**
 * @date 2017-05-04
 * @time 17:13
 * @author n.dashutin <mikola.dashutin@gmail.com>
 */

namespace frontend\application\exception;

/**
 * Class UserAlreadyRegisteredException
 * @author n.dashutin <mikola.dashutin@gmail.com>
 * @package frontend\application\exception
 */
class UserAlreadyRegisteredException extends ApplicationException
{

}