<?php
/**
 * @date 2017-05-04
 * @time 20:52
 * @author n.dashutin <mikola.dashutin@gmail.com>
 */

namespace frontend\application\exception;

/**
 * Class UndefinedEmailTemplateException
 * @author n.dashutin <mikola.dashutin@gmail.com>
 * @package frontend\application\exception
 */
class UndefinedEmailTemplateException extends ApplicationException
{

}