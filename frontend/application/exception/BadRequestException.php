<?php
/**
 * @date 2017-05-03
 * @time 20:01
 * @author n.dashutin <mikola.dashutin@gmail.com>
 */

namespace frontend\application\exception;

/**
 * Class BadRequestException
 * @author n.dashutin <mikola.dashutin@gmail.com>
 * @package frontend\application\exception
 */
class BadRequestException extends ApplicationException
{

}