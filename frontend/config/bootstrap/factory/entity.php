<?php
/**
 * @date 2017-05-04
 * @time 15:56
 * @author n.dashutin <mikola.dashutin@gmail.com>
 */
$container = \Yii::$container;

$container->set(
    'userEntityFactory',
    \frontend\application\common\Factory\Entity\EntityFactory::class,
    [new \frontend\application\domain\Entity\User\UserEntity]
);

$container->set(
    'userAuthKeyEntityFactory',
    \frontend\application\common\Factory\Entity\EntityFactory::class,
    [new \frontend\application\domain\Entity\UserAuthenticationKey\UserAuthenticationKeyEntity]
);