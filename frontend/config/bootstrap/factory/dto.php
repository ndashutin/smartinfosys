<?php
/**
 * @date 2017-05-04
 * @time 15:16
 * @author n.dashutin <mikola.dashutin@gmail.com>
 */
$container = \Yii::$container;

$container->set(
    'authenticationDtoFactory',
    \frontend\application\common\Factory\Entity\EntityFactory::class,
    [new \frontend\application\domain\DTO\Authentication\AuthenticationDTO()]
);

$container->set(
    'registerDtoFactory',
    \frontend\application\common\Factory\Entity\EntityFactory::class,
    [new \frontend\application\domain\DTO\Registration\RegistrationDTO()]
);