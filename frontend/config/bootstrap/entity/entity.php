<?php
/**
 * @date 2017-05-04
 * @time 15:57
 * @author n.dashutin <mikola.dashutin@gmail.com>
 */
$container = \Yii::$container;

$container->set('userEntity', \frontend\application\domain\Entity\User\UserEntity::class);
$container->set(
    'userAuthKeyEntity',
    \frontend\application\domain\Entity\UserAuthenticationKey\UserAuthenticationKeyEntity::class
);