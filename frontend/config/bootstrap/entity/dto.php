<?php
/**
 * @date 2017-05-04
 * @time 15:27
 * @author n.dashutin <mikola.dashutin@gmail.com>
 */
$container = \Yii::$container;

$container->set(
    \frontend\application\domain\DTO\Authentication\AuthenticationDTOInterface::class,
    \frontend\application\domain\DTO\Authentication\AuthenticationDTO::class
);

$container->set(
    \frontend\application\domain\DTO\Registration\RegistrationDTOInterface::class,
    \frontend\application\domain\DTO\Registration\RegistrationDTO::class
);