<?php
/**
 * @date 2017-05-04
 * @time 15:30
 * @author n.dashutin <mikola.dashutin@gmail.com>
 */
$container = \Yii::$container;

$container->set(
    \frontend\controller\FrontController::class,
    \frontend\controller\FrontController::class,
    [
        null,
        null,
        $container->get('authenticationDtoFactory'),
        $container->get('registerDtoFactory'),
    ]
);

$container->set(
    \frontend\controller\ExternalSignInController::class,
    \frontend\controller\ExternalSignInController::class,
    [
        null,
        null,
        $container->get(\frontend\application\services\AuthenticationService\AuthenticationServiceInterface::class),
        $container->get('authenticationDtoFactory')
    ]
);

$container->set(
    \frontend\controller\ExternalSignUpController::class,
    \frontend\controller\ExternalSignUpController::class,
    [
        null,
        null,
        $container->get(\frontend\application\services\RegistrationService\RegistrationServiceInterface::class),
        $container->get('registerDtoFactory')
    ]
);