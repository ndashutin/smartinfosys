<?php
/**
 * @date 2017-05-05
 * @time 07:40
 * @author n.dashutin <mikola.dashutin@gmail.com>
 */
$container = \Yii::$container;

$container->set(
    \frontend\application\services\NotificationService\Email\EmailTemplateProviderInterface::class,
    \frontend\models\Notification\Email\StaticEmailTemplateProvider::class
);