<?php
/**
 * @date 2017-05-04
 * @time 15:50
 * @author n.dashutin <mikola.dashutin@gmail.com>
 */
$container = \Yii::$container;

$container->set(
    \frontend\application\services\PasswordService\PasswordServiceInterface::class,
    \frontend\application\services\PasswordService\PasswordService::class
);