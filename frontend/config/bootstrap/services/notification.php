<?php
/**
 * @date 2017-05-05
 * @time 07:31
 * @author n.dashutin <mikola.dashutin@gmail.com>
 */
$container = \Yii::$container;

$container->set(
    \frontend\application\services\NotificationService\NotificationServiceInterface::class,
    \frontend\application\services\NotificationService\NotificationService::class
);