<?php
/**
 * @date 2017-05-04
 * @time 15:44
 * @author n.dashutin <mikola.dashutin@gmail.com>
 */
$container = \Yii::$container;

$container->set(
    \frontend\application\services\AuthenticationService\AuthenticationServiceInterface::class,
    \frontend\application\services\AuthenticationService\AuthenticationService::class
);