<?php
/**
 * @date 2017-05-04
 * @time 16:26
 * @author n.dashutin <mikola.dashutin@gmail.com>
 */
$container = \Yii::$container;

$container->set(
    \frontend\application\services\RegistrationService\RegistrationServiceInterface::class,
    \frontend\application\services\RegistrationService\RegistrationService::class
);