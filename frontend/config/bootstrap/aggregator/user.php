<?php
/**
 * @date 2017-05-04
 * @time 15:45
 * @author n.dashutin <mikola.dashutin@gmail.com>
 */
$container = \Yii::$container;

$container->set(
    \frontend\application\domain\Aggregator\UserDataAggregatorInterface::class,
    \frontend\models\Aggregator\UserDataAggregator::class,
    [
        $container->get('userRepository'),
        $container->get('userAuthKeyRepository')
    ]
);