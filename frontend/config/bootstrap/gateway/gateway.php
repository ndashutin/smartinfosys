<?php
/**
 * @date 2017-05-04
 * @time 15:52
 * @author n.dashutin <mikola.dashutin@gmail.com>
 */
$container = \Yii::$container;

$container->set('userGateway', \frontend\models\User\UserGateway::class);
$container->set('userAuthKeyGateway', \frontend\models\UserAuthenticationKey\UserAuthenticationKeyGateway::class);