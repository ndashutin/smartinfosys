<?php
/**
 * @date 2017-05-05
 * @time 07:32
 * @author n.dashutin <mikola.dashutin@gmail.com>
 */
$container = \Yii::$container;

$container->set(
    \frontend\application\domain\MessageBroker\MessageBrokerInterface::class,
    \frontend\models\Job\RabbitMqMessageBroker::class
);

$container->set(
    \frontend\models\Job\MessageBrokerJobFactoryInterface::class,
    \frontend\models\Job\MessageBrokerJobFactory::class
);