<?php
/**
 * @date 2017-04-29
 * @time 17:42
 * @author n.dashutin <mikola.dashutin@gmail.com>
 */
$container = \Yii::$container;
$container->set(
    \frontend\application\dao\EntityMapperInterface::class,
    \frontend\application\dao\EntityMapper::class
);
require __DIR__ . '/entity/entity.php';
require __DIR__ . '/entity/dto.php';
require __DIR__ . '/factory/entity.php';
require __DIR__ . '/factory/dto.php';
require __DIR__ . '/gateway/gateway.php';
require __DIR__ . '/providers/providers.php';
require __DIR__ . '/services/password.php';
require __DIR__ . '/repository/repository.php';
require __DIR__ . '/aggregator/user.php';
require __DIR__ . '/broker/broker.php';
require __DIR__ . '/services/notification.php';
require __DIR__ . '/services/authenticate.php';
require __DIR__ . '/services/registration.php';
require __DIR__ . '/routes/controller.php';