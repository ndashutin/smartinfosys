<?php
/**
 * @date 2017-05-04
 * @time 15:47
 * @author n.dashutin <mikola.dashutin@gmail.com>
 */
$container = \Yii::$container;

$container->set(
    'userRepository',
    \frontend\application\common\Repository\Repository::class,
    [
        $container->get('userGateway'),
        $container->get(\frontend\application\dao\EntityMapperInterface::class),
        $container->get('userEntityFactory')
    ]
);

\frontend\models\User\UserIdentityObject::$repository = $container->get('userRepository');

$container->set(
    'userAuthKeyRepository',
    \frontend\application\dao\UserAuthKey\UserAuthKeyRepository::class,
    [
        $container->get('userAuthKeyGateway'),
        $container->get(\frontend\application\dao\EntityMapperInterface::class),
        $container->get('userAuthKeyEntityFactory')
    ]
);