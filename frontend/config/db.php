<?php use yii\db\Connection;

/**
 * @date 2017-04-29
 * @time 16:06
 * @author n.dashutin <mikola.dashutin@gmail.com>
 */
$pathToDB = __DIR__ . '/../db/database.db';
return [
    'class' => Connection::class,
    'dsn' => 'sqlite:' . $pathToDB,
];