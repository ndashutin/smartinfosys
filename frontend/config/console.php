<?php
/**
 * @date 2017-04-29
 * @time 16:02
 * @author n.dashutin <mikola.dashutin@gmail.com>
 */
return [
    'id' => 'smartinfosys-console',
    'language' => 'en-GB',
    'name' => 'SmartInfoSys',
    'basePath' => __DIR__ . '/../',
    'bootstrap' => ['broker'],
    'components' => [
        'db' => require __DIR__ . '/db.php',
        'broker' => require __DIR__ . '/broker.php',
    ]
];