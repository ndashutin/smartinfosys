<?php
use yii\authclient\clients\Facebook;
use yii\authclient\Collection;
use yii\debug\Module;

/**
 * @date 2017-04-29
 * @time 13:51
 * @author n.dashutin <mikola.dashutin@gmail.com>
 */
Yii::setAlias('@frontend', __DIR__ . '/../');
require __DIR__ . '/bootstrap/application.php';
return [
    'id' => 'smartinfosys',
    'language' => 'en-GB',
    'name' => 'SmartInfoSys',
    'basePath' => __DIR__ . '/../',
    'bootstrap' => defined('YII_DEBUG') && YII_DEBUG ? ['debug', 'broker'] : ['broker'],
    'controllerNamespace' => 'frontend\controller',
    'vendorPath' => __DIR__ . '/../../vendor',
    'defaultRoute' => 'front/index',
    'modules' => defined('YII_DEBUG') && YII_DEBUG ? ['debug' => ['class' => Module::class]] : [],
    'components' => array_merge(
        [
            'db' => require __DIR__ . '/db.php',
            'broker' => require __DIR__ . '/broker.php',
            'user' => [
                'identityClass' => \frontend\models\User\UserIdentityObject::class
            ],
            'errorHandler' => [
                'errorAction' => 'front/error',
            ],
            'urlManager' => [
                'enablePrettyUrl' => true,
                'showScriptName' => false,
            ],
            'request' => [
                'cookieValidationKey' => 'mhR4ykocmrxX37vlxiqXa3usa5Xara',
            ],
            'authClientCollection' => [
                'class' => Collection::class,
                'clients' => [
                    'facebook' => [
                        'class' => Facebook::class,
                        'authUrl' => 'https://www.facebook.com/dialog/oauth?display=popup',
                        'clientId' => '124728391410760',
                        'clientSecret' => '6fcc8d4c3756d1b7b8264e4f1bb0a3be',
                        'attributeNames' => ['email']
                    ],
                ],
            ],
        ]
    ),
];