<?php
/**
 * @date 2017-05-04
 * @time 20:26
 * @author n.dashutin <mikola.dashutin@gmail.com>
 */
/*if (defined('YII_DEBUG')) {
    return [
        'class' => \zhuravljov\yii\queue\file\Queue::class,
        'path' => '@runtime/queue',
    ];
}*/

return [
    'class' => \zhuravljov\yii\queue\amqp\Queue::class,
    'host' => '127.0.0.1',
    'port' => 5672,
    'user' => 'mailer',
    'password' => 'mailer',
    'exchangeName' => 'emailDirect',
    'queueName' => 'email',
];