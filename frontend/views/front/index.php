<?php
/**
 * @date 2017-04-29
 * @time 14:00
 * @author n.dashutin <mikola.dashutin@gmail.com>
 */
/**
 * @var \frontend\models\User\UserForm $form
 * @var string $action
 */
?>
<div class="container margin-top-5-p">
    <div class="row">
        <div class="col-lg-4 col-sm-10 col-md-3 col-lg-offset-4 col-md-offset-3 col-sm-offset-1">
            <?php if (\Yii::$app->user->isGuest) : ?>
                <?= \yii\bootstrap\Tabs::widget([
                    'items' => [
                        [
                            'label' => \rmrevin\yii\fontawesome\FA::i('sign-in') . ' Sign In',
                            'content' => \frontend\widgets\SignIn\SignInFormWidget::widget(['form' => $form]),
                            'active' => $action === 'sign-in',
                            'encode' => false
                        ],
                        [
                            'label' => \rmrevin\yii\fontawesome\FA::i('user-plus') . ' Sign Up',
                            'content' => \frontend\widgets\SignUp\SignUpFormWidget::widget(['form' => $form]),
                            'active' => $action === 'sign-up',
                            'encode' => false
                        ]
                    ],
                ]); ?>
            <?php else : ?>
                <div class="text-center">
                    <p>
                        <a href="<?= \yii\helpers\Url::toRoute('/front/sign-out') ?>" class="btn btn-lg btn-info">
                            <i class="fa fa-sign-out" aria-hidden="true"></i>
                            Logout
                        </a>
                    </p>
                    <a href="<?= \yii\helpers\Url::toRoute('/front/delete') ?>" class="btn btn-danger">
                        <i class="fa fa-trash" aria-hidden="true"></i>
                        Delete this user
                    </a>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>
