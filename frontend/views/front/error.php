<?php
/**
 * @date 2017-04-29
 * @time 18:30
 * @author n.dashutin <mikola.dashutin@gmail.com>
 */
/**
 * @var \yii\web\View $this
 * @var string $name
 * @var string $message
 * @var \yii\base\Exception $exception
 */
?>

<div class="container margin-top-5-p">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h1 style="color: red" class="text-center">
                <b>Error <?= $name ?></b>
                <?= $message ?>
            </h1>
        </div>
        <div class="panel-body">
            <?php if (defined('YII_ENV') && YII_ENV === 'dev') : ?>
                <code>
                    File : <?= $exception->getFile() ?>
                    <br>
                    Line : <?= $exception->getLine() ?>
                </code>
            <?php else : ?>
                <div class="text-center">
                    <code>Something went wrong. Try reload page!</code>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>