<?php
/**
 * @date 2017-04-29
 * @time 14:25
 * @author n.dashutin <mikola.dashutin@gmail.com>
 */
/**
 * @var string $content
 * @var \yii\web\View $this
 */
$this->title = 'fb-test';
\frontend\assets\FrontendAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= \yii\bootstrap\Html::csrfMetaTags() ?>
    <title><?= \yii\bootstrap\Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<div style="position: fixed; top: 1%; width: 100%">
    <div class="col-lg-12">
        <?php if (Yii::$app->session->hasFlash('error')): ?>
            <div class="alert alert-danger alert-dismissible text-center" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
                <?= Yii::$app->session->getFlash('error') ?>
            </div>
        <?php endif; ?>
    </div>
</div>
<?php $this->beginBody() ?>
<?= $content ?>
<div style="position: fixed; right: 1%; bottom: 0; color: #a5a5a5; cursor: pointer" title="mikola.dashutin@gmail.com">
    N.Dashutin &copy; <?= date('Y') ?>
</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
