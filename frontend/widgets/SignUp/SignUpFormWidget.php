<?php
/**
 * @date 2017-04-29
 * @time 17:51
 * @author n.dashutin <mikola.dashutin@gmail.com>
 */

namespace frontend\widgets\SignUp;

use frontend\models\User\UserForm;
use yii\bootstrap\Widget;

/**
 * Class SignUpFormWidget
 * @author n.dashutin <mikola.dashutin@gmail.com>
 * @package frontend\widgets\SignUp
 */
class SignUpFormWidget extends Widget
{
    /**
     * @var UserForm
     */
    public $form;

    /**
     * @inheritDoc
     * @throws \yii\base\InvalidParamException
     */
    public function run(): string
    {
        return $this->render('form', ['user' => $this->form]);
    }
}