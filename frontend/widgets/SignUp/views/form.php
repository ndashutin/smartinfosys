<?php
/**
 * @date 2017-04-29
 * @time 17:52
 * @author n.dashutin <mikola.dashutin@gmail.com>
 */
/**
 * @var \frontend\models\User\UserForm $user
 */
?>
<div class="padding-all-5-px">
    <?php $form = \yii\bootstrap\ActiveForm::begin(['action' => \yii\helpers\Url::toRoute('front/sign-up')]) ?>
    <?= $form->field($user, 'email')->textInput(['autofocus' => true]) ?>
    <?= $form->field($user, 'password')->passwordInput() ?>
    <?= \yii\bootstrap\Html::submitButton(
        'Sign Up',
        [
            'class' => 'btn btn-success'
        ]
    ) ?>
    <?php $form::end() ?>
    <div class="margin-top-5-p">
        <div class="col-lg-12 text-info bg-info text-center">
            <b>Or sign up with the help of social networks</b>
        </div>
        <div class="col-lg-12 text-center margin-top-5-p">
            <?= yii\authclient\widgets\AuthChoice::widget([
                'baseAuthUrl' => ['external-sign-up/sign-up'],
                'popupMode' => true
            ]) ?>
        </div>
    </div>
</div>

