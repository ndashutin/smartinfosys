<?php
/**
 * @date 2017-04-29
 * @time 17:21
 * @author n.dashutin <mikola.dashutin@gmail.com>
 */

namespace frontend\widgets\SignIn;

use frontend\models\User\UserForm;
use yii\bootstrap\Widget;

/**
 * Class SignInFormWidget
 * @author n.dashutin <mikola.dashutin@gmail.com>
 * @package frontend\widgets\SignIn
 */
class SignInFormWidget extends Widget
{
    /**
     * @var UserForm
     */
    public $form;

    /**
     * @inheritDoc
     * @throws \yii\base\InvalidParamException
     */
    public function run(): string
    {
        return $this->render('form', ['user' => $this->form]);
    }

}