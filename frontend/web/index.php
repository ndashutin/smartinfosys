<?php

namespace frontend\web;

/**
 * @date 2017-04-29
 * @time 13:52
 * @author n.dashutin <mikola.dashutin@gmail.com>
 */

use yii\web\Application;

defined('YII_DEBUG') || define('YII_DEBUG', false);
defined('YII_ENV') || define('YII_ENV', 'prod');

require __DIR__ . '/../../vendor/autoload.php';
require __DIR__ . '/../../vendor/yiisoft/yii2/Yii.php';

$config = require __DIR__ . '/../config/web.php';
(new Application($config))->run();