<?php
/**
 * @date 2017-05-04
 * @time 20:51
 * @author n.dashutin <mikola.dashutin@gmail.com>
 */

namespace frontend\models\Job;

use frontend\application\domain\Job\MessageBrokerJobInterface;

/**
 * Class MessageBrokerJobFactory
 * @author n.dashutin <mikola.dashutin@gmail.com>
 * @package frontend\models\Job
 */
interface MessageBrokerJobFactoryInterface
{
    /**
     * Метод makeJob
     * @author n.dashutin <mikola.dashutin@gmail.com>
     * @param $jobType
     * @return MessageBrokerJobInterface
     * @throws \frontend\application\exception\UndefinedMessageBrokerJobTypeException
     */
    public function makeJob($jobType);
}