<?php
/**
 * @date 2017-05-04
 * @time 20:17
 * @author n.dashutin <mikola.dashutin@gmail.com>
 */

namespace frontend\models\Job\User;

use frontend\application\domain\Job\MessageBrokerJobInterface;
use zhuravljov\yii\queue\Job;

/**
 * Class UserAfterRegistrationNotify
 * @author n.dashutin <mikola.dashutin@gmail.com>
 * @package frontend\models\Job\User
 */
class UserAfterRegistrationNotify implements Job, MessageBrokerJobInterface
{
    /**
     * @var string
     */
    public $receiver;
    /**
     * @var string
     */
    public $body;
    /**
     * @var string
     */
    public $title;

    /**
     * @inheritDoc
     */
    public function execute($queue)
    {
        echo '|----------------------------------------------------------------------------------------------' . PHP_EOL;
        echo date('Y-m-d G:i:s') . PHP_EOL;
        echo 'New email for: ' . $this->receiver . PHP_EOL;
        echo 'Title: ' . $this->title . PHP_EOL;
        echo $this->body . PHP_EOL;
        echo '|----------------------------------------------------------------------------------------------' . PHP_EOL;
        echo PHP_EOL;
    }

    /**
     * @inheritDoc
     */
    public function setContext(array $context)
    {
        $this->receiver = $context['receiver'] ?? '';
        $this->body = $context['body'] ?? '';
        $this->title = $context['title'] ?? '';
    }


}