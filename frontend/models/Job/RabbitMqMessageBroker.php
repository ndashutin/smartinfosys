<?php
/**
 * @date 2017-05-04
 * @time 20:38
 * @author n.dashutin <mikola.dashutin@gmail.com>
 */

namespace frontend\models\Job;

use frontend\application\domain\Job\MessageBrokerJobInterface;
use frontend\application\domain\MessageBroker\MessageBrokerInterface;

/**
 * Class RabbitMqMessageBroker
 * @author n.dashutin <mikola.dashutin@gmail.com>
 * @package frontend\models\Job
 */
class RabbitMqMessageBroker implements MessageBrokerInterface
{
    /**
     * @inheritDoc
     */
    public function sendMessage(MessageBrokerJobInterface $job)
    {
        \Yii::$app->get('broker')->push($job);
    }
}