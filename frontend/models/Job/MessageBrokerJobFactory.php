<?php
/**
 * @date 2017-05-04
 * @time 20:41
 * @author n.dashutin <mikola.dashutin@gmail.com>
 */

namespace frontend\models\Job;

use frontend\application\exception\UndefinedMessageBrokerJobTypeException;
use frontend\models\Job\User\UserAfterRegistrationNotify;

/**
 * Class MessageBrokerJobFactory
 * @author n.dashutin <mikola.dashutin@gmail.com>
 * @package frontend\models\Job
 */
class MessageBrokerJobFactory implements MessageBrokerJobFactoryInterface
{
    /**
     * @inheritdoc
     */
    public function makeJob($jobType)
    {
        switch ($jobType) {
            case 'afterUserRegistration':
                return new UserAfterRegistrationNotify;
            default :
                throw new UndefinedMessageBrokerJobTypeException(
                    'Unknown message job type "' . $jobType . '"!'
                );
        }
    }
}