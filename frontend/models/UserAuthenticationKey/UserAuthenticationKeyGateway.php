<?php
/**
 * @date 2017-05-03
 * @time 21:37
 * @author n.dashutin <mikola.dashutin@gmail.com>
 */

namespace frontend\models\UserAuthenticationKey;

use frontend\application\common\Gateway\GatewayInterface;
use frontend\models\ActiveRecordGateway;

/**
 * Class UserAuthenticationKeyGateway
 * @author n.dashutin <mikola.dashutin@gmail.com>
 * @package frontend\models\UserAuthenticationKey
 */
class UserAuthenticationKeyGateway extends ActiveRecordGateway implements GatewayInterface
{
    /**
     * @inheritdoc
     */
    public static function tableName(): string
    {
        return 'userAuthenticationKey';
    }

    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [['userId', 'providerUserId'], 'required'],
            [['id', 'userId'], 'integer'],
            [['providerUserId'], 'string']
        ];
    }
}