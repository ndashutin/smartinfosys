<?php
/**
 * @date 2017-05-03
 * @time 21:29
 * @author n.dashutin <mikola.dashutin@gmail.com>
 */

namespace frontend\models;

use frontend\application\exception\GatewayException;
use yii\db\ActiveRecord;

/**
 * Class ActiveRecordGateway
 * @author n.dashutin <mikola.dashutin@gmail.com>
 * @package frontend\models
 */
abstract class ActiveRecordGateway extends ActiveRecord
{
    /**
     * Метод getOneByCriteria
     * @author n.dashutin <mikola.dashutin@gmail.com>
     * @param array $criteria
     * @return array
     */
    public function getOneByCriteria(array $criteria): array
    {
        $entity = parent::findOne($criteria);
        if (!$entity) {
            return [];
        }
        return $entity->getAttributes();
    }

    /**
     * Метод getByCriteria
     * @author n.dashutin <mikola.dashutin@gmail.com>
     * @param array $criteria
     * @return array
     */
    public function getByCriteria(array $criteria = []): array
    {
        $result = [];
        $entities = parent::findAll($criteria);
        if (is_array($entities) && count($entities)) {
            foreach ($entities as $entity) {
                $result[] = $entity->getAttributes();
            }
        }
        return $result;
    }

    /**
     * Метод create
     * @author n.dashutin <mikola.dashutin@gmail.com>
     * @param array $data
     * @return bool
     * @throws \yii\base\InvalidParamException
     * @throws \frontend\application\exception\GatewayException
     */
    public function create(array $data): bool
    {
        return $this->updateOne($data);
    }

    /**
     * Метод updateOne
     * @author n.dashutin <mikola.dashutin@gmail.com>
     * @param array $data
     * @return bool
     * @throws \frontend\application\exception\GatewayException
     * @throws \yii\base\InvalidParamException
     */
    public function updateOne(array $data): bool
    {
        $this->loadDaoEntity($this, $data);
        if ($this->validate()) {
            if (isset($data['id'])) {
                $entity = parent::findOne(['id' => $data['id']]);
                $this->loadDaoEntity($entity, $data);
                return $entity->save();
            }
            return $this->save();
        }
        $message = 'Invalid parameters for save data in gateway!';
        $validationErrorMessage = $this->getValidationErrorMessage();
        if (null === $validationErrorMessage) {
            $message .= '' . $validationErrorMessage;
        }
        throw new GatewayException($message);
    }

    /**
     * Метод loadDaoEntity
     * @author n.dashutin <mikola.dashutin@gmail.com>
     * @param ActiveRecord $entity
     * @param array $data
     * @return ActiveRecord
     * @throws \yii\base\InvalidParamException
     */
    private function loadDaoEntity(ActiveRecord $entity, array $data)
    {
        foreach ($data as $attributeName => $attributeValue) {
            if (null !== $attributeValue) {
                $entity->setAttribute($attributeName, $attributeValue);
            }
        }
        return $entity;
    }

    /**
     * Метод getValidationErrorMessage
     * @author n.dashutin <mikola.dashutin@gmail.com>
     * @return string|null
     */
    protected function getValidationErrorMessage()
    {
        if ($this->hasErrors()) {
            $errorsArray = $this->getErrors();
            return array_shift($errorsArray);
        }
        return null;
    }

    /**
     * Метод remove
     * @author n.dashutin <mikola.dashutin@gmail.com>
     * @param array $criteria
     * @return bool
     */
    public function remove(array $criteria): bool
    {
        return parent::deleteAll($criteria);
    }
}