<?php
/**
 * @date 2017-05-03
 * @time 20:34
 * @author n.dashutin <mikola.dashutin@gmail.com>
 */

namespace frontend\models\User;

use frontend\application\common\Repository\RepositoryInterface;
use frontend\application\domain\Entity\User\UserEnum;
use frontend\application\domain\Entity\User\UserInterface;
use yii\web\IdentityInterface;

/**
 * Class UserIdentityObject
 * @author n.dashutin <mikola.dashutin@gmail.com>
 * @package frontend\models\User
 */
class UserIdentityObject implements IdentityInterface
{
    /**
     * @var RepositoryInterface
     */
    public static $repository;
    /**
     * @var UserInterface
     */
    private $currentUser;

    /**
     * UserIdentityObject constructor.
     * @param UserInterface $currentUser
     */
    public function __construct(UserInterface $currentUser = null)
    {
        $this->setCurrentUser($currentUser);
    }

    /**
     * Метод setCurrentUser
     * @author n.dashutin <mikola.dashutin@gmail.com>
     * @param UserInterface $currentUser
     */
    public function setCurrentUser(UserInterface $currentUser)
    {
        $this->currentUser = $currentUser;
    }

    /**
     * @inheritDoc
     */
    public static function findIdentity($id): IdentityInterface
    {
        $user = static::$repository->getOne(['id' => $id, 'statusId' => UserEnum::STATUS_ACTIVE]);
        if ($user) {
            return new static($user);
        }
        return null;
    }

    /**
     * @inheritDoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        $user = static::$repository->getOne(['accessToken' => $token, 'statusId' => UserEnum::STATUS_ACTIVE]);
        if ($user) {
            return new static($user);
        }
        return null;
    }

    /**
     * @inheritDoc
     */
    public function getId()
    {
        return $this->currentUser->getId();
    }

    public function getEmail()
    {
        return $this->currentUser->getEmail();
    }

    /**
     * @inheritDoc
     */
    public function getAuthKey()
    {
        return '';
    }

    /**
     * @inheritDoc
     */
    public function validateAuthKey($authKey)
    {
        return true;
    }

}