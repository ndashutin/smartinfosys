<?php
/**
 * @date 2017-05-04
 * @time 14:53
 * @author n.dashutin <mikola.dashutin@gmail.com>
 */

namespace frontend\models\User;

/**
 * Class UserForm
 * @author n.dashutin <mikola.dashutin@gmail.com>
 * @package frontend\models\User
 */
class UserForm extends UserGateway
{
    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [['email', 'password'], 'required'],
            [['statusId', 'id'], 'integer'],
            [['email', 'password'], 'string'],
            [['email', 'password'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels(): array
    {
        return [
            'email' => 'Email',
            'password' => 'Password'
        ];
    }
}