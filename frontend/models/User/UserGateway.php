<?php
/**
 * @date 2017-05-03
 * @time 21:28
 * @author n.dashutin <mikola.dashutin@gmail.com>
 */

namespace frontend\models\User;

use frontend\application\common\Gateway\GatewayInterface;
use frontend\models\ActiveRecordGateway;

/**
 * Class UserGateway
 * @author n.dashutin <mikola.dashutin@gmail.com>
 * @package frontend\models\User
 */
class UserGateway extends ActiveRecordGateway implements GatewayInterface
{
    /**
     * @inheritdoc
     */
    public static function tableName(): string
    {
        return 'user';
    }
}