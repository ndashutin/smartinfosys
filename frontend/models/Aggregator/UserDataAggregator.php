<?php
/**
 * @date 2017-05-04
 * @time 00:22
 * @author n.dashutin <mikola.dashutin@gmail.com>
 */

namespace frontend\models\Aggregator;

use frontend\application\common\Repository\RepositoryInterface;
use frontend\application\domain\Aggregator\UserDataAggregatorInterface;
use frontend\application\domain\Entity\User\UserEnum;
use frontend\application\domain\Entity\User\UserInterface;
use frontend\application\domain\Entity\UserAuthenticationKey\UserAuthenticationKeyEntityInterface;
use frontend\application\services\PasswordService\PasswordServiceInterface;

/**
 * Class UserDataAggregator
 * @author n.dashutin <mikola.dashutin@gmail.com>
 * @package frontend\models\Authentication
 */
class UserDataAggregator implements UserDataAggregatorInterface
{
    /**
     * @var RepositoryInterface
     */
    private $userRepository;
    /**
     * @var RepositoryInterface
     */
    private $userAuthenticationKeyRepository;
    /**
     * @var PasswordServiceInterface
     */
    private $passwordService;

    /**
     * UserDataProvider constructor.
     * @param RepositoryInterface $userRepository
     * @param RepositoryInterface $userAuthenticationKeyRepository
     * @param PasswordServiceInterface $passwordService
     */
    public function __construct(
        RepositoryInterface $userRepository,
        RepositoryInterface $userAuthenticationKeyRepository,
        PasswordServiceInterface $passwordService
    )
    {
        $this->userRepository = $userRepository;
        $this->userAuthenticationKeyRepository = $userAuthenticationKeyRepository;
        $this->passwordService = $passwordService;
    }


    /**
     * @inheritDoc
     */
    public function getUserByAuthenticationId($authenticationId)
    {
        return $this->userRepository->getOne(['email' => $authenticationId, 'statusId' => UserEnum::STATUS_ACTIVE]);
    }

    /**
     * @inheritDoc
     */
    public function getUserAuthenticationKey(UserInterface $user, $externalAuthenticationProviderUserId)
    {
        return $this->userAuthenticationKeyRepository->getOne(
            [
                'userId' => $user->getId(),
                'providerUserId' => $externalAuthenticationProviderUserId
            ]
        );
    }

    /**
     * @inheritDoc
     */
    public function getUserByAuthenticationIdAndOpenPassword($authenticationId, $openPassword)
    {
        return $this->userRepository->getOne(
            [
                'email' => $authenticationId,
                'password' => $this->passwordService->makePasswordHash($openPassword),
                'statusId' => UserEnum::STATUS_ACTIVE
            ]
        );
    }

    /**
     * @inheritDoc
     */
    public function setNewUserAuthenticationKey(UserInterface $user, $externalAuthenticationProviderUserId): UserAuthenticationKeyEntityInterface
    {
        $newUserAuthenticationKey = $this->userAuthenticationKeyRepository->makeEntityFromArray([
            'userId' => $user->getId(),
            'providerUserId' => $externalAuthenticationProviderUserId
        ]);
        $this->userAuthenticationKeyRepository->create($newUserAuthenticationKey);
        return $this->userAuthenticationKeyRepository->getOne([
            'userId' => $user->getId(),
            'providerUserId' => $externalAuthenticationProviderUserId
        ]);
    }

    /**
     * @inheritDoc
     */
    public function setNewUser($authenticationId, $openPassword = null): UserInterface
    {
        if (empty($openPassword)) {
            $openPassword = $this->generateNewPassword();
        }
        $properties = [
            'email' => $authenticationId,
            'password' => $this->passwordService->makePasswordHash($openPassword)
        ];
        $newUser = $this->userRepository->makeEntityFromArray($properties);
        $this->userRepository->create($newUser);
        return $this->userRepository->getOne($properties);
    }

    /**
     * Метод generateNewPassword
     * @author n.dashutin <mikola.dashutin@gmail.com>
     * @return string
     */
    public function generateNewPassword(): string
    {
        return $this->passwordService->generateRandom();
    }

    /**
     * Метод deleteUser
     * @author n.dashutin <mikola.dashutin@gmail.com>
     * @param UserInterface $user
     * @return mixed
     */
    public function deleteUser(UserInterface $user)
    {
        $userEntity = $this->userRepository->makeEntityFromArray(
            [
                'id' => $user->getId(),
                'password' => $user->getPassword(),
                'email' => $user->getEmail(),
                'statusId' => UserEnum::STATUS_DELETED
            ]
        );
        return $this->userRepository->update($userEntity);
    }

}