<?php
/**
 * @date 2017-05-05
 * @time 07:01
 * @author n.dashutin <mikola.dashutin@gmail.com>
 */

namespace frontend\models\Notification\Email\Template;

use frontend\application\services\NotificationService\Email\Template\EmailTemplateInterface;

/**
 * Class EmailTemplate
 * @author n.dashutin <mikola.dashutin@gmail.com>
 * @package frontend\models\Notification\Email\Template
 */
class AfterUserRegistrationEmailTemplate implements EmailTemplateInterface
{
    /**
     * @const string
     */
    const INTERPOLATE_LEFT_SYMBOL = '{';
    /**
     * @const string
     */
    const INTERPOLATE_RIGHT_SYMBOL = '}';

    /**
     * @var string
     */
    private $body;
    /**
     * @var string
     */
    private $title;

    /**
     * AfterUserRegistrationEmailTemplate constructor.
     * @param string $body
     * @param string $title
     */
    public function __construct($body, $title = 'Successful registration')
    {
        $this->body = $body;
        $this->title = $title;
    }

    /**
     * Метод build
     * @author n.dashutin <mikola.dashutin@gmail.com>
     * @param array $contextArray
     */
    public function build(array $contextArray = [])
    {
        $bodyPatternString = $this->body;
        if (count($contextArray)) {
            $replace = [];
            foreach ($contextArray as $logDataParameterName => $logDataParameterValue) {
                if (!is_array($logDataParameterValue)) {
                    $formula =
                        static::INTERPOLATE_LEFT_SYMBOL
                        . $logDataParameterName
                        . static::INTERPOLATE_RIGHT_SYMBOL;
                    $replace[$formula] = $logDataParameterValue;
                }
            }
            $this->body = strtr($bodyPatternString, $replace);
        } else {
            $this->body = $bodyPatternString;
        }
    }

    /**
     * @return string
     */
    public function getBody(): string
    {
        return (string)$this->body;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }
}