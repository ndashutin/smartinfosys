<?php
/**
 * @date 2017-05-04
 * @time 20:50
 * @author n.dashutin <mikola.dashutin@gmail.com>
 */

namespace frontend\models\Notification\Email;

use frontend\application\exception\UndefinedEmailTemplateException;
use frontend\application\services\NotificationService\Email\EmailTemplateProviderInterface;
use frontend\application\services\NotificationService\Email\Template\EmailTemplateInterface;
use frontend\models\Notification\Email\Template\AfterUserRegistrationEmailTemplate;

/**
 * Class StaticEmailTemplateProvider
 * @author n.dashutin <mikola.dashutin@gmail.com>
 * @package frontend\models\Notification\Email
 */
class StaticEmailTemplateProvider implements EmailTemplateProviderInterface
{
    /**
     * @inheritDoc
     */
    public function provide($emailTemplateTitle): EmailTemplateInterface
    {
        switch ($emailTemplateTitle) {
            case 'afterUserRegistration':
                return new AfterUserRegistrationEmailTemplate($this->afterUserRegistrationEmailText());
            default:
                throw new UndefinedEmailTemplateException(
                    'Unknown email template title "' . $emailTemplateTitle . '"!'
                );
        }
    }

    /**
     * Метод afterUserRegistrationEmailText
     * @author n.dashutin <mikola.dashutin@gmail.com>
     * @return string
     */
    private function afterUserRegistrationEmailText(): string
    {
        $message = 'Hello {authenticateId}' . "\n";
        $message .= 'You login: {authenticateId}' . "\n";
        $message .= 'You password: {password}';
        return $message;
    }

}