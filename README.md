# Facebook authentication #

Standard Authorization with Registration + 
Authorization and registration via Facebook

Asynchronous e-mail sending upon successful registration

Start queue listener

```
php yii broker/listen
```

DEMO URL http://107.170.97.58

### Queue ###

URL http://107.170.97.58:15672
Login: monitor
Password: monitor

### Queue Log ###

URL http://107.170.97.58:8080/queue.log
### Requirements ###
PHP 7.0,
mbstring mod,
bcmath mod,
PDO: PDO_SQLITE


### Installation ###

**Copy**
```
git clone https://bitbucket.org/ndashutin/smartinfosys.git
```
**Install dependencies**

On local server
```
composer -o --prefer-dist install
```
In production
```
composer -o --prefer-dist --no-dev install
```

**Enable write access for directories**
```
chmode -R 0755 frontend/db
chmode -R 0755 frontend/web/assets
```
**Run migrations**
```
php yii migrate
```
